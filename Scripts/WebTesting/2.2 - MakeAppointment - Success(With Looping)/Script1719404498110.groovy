import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import ch.qos.logback.core.joran.conditional.ElseAction
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory

//Get test data from Excel file
def appoinmentData = TestDataFactory.findTestData('Data Files/AppointmentData')

//Define Variables
for (int row = 1; row <= appoinmentData.getRowNumbers(); row++) {
	String facility = appoinmentData.getValue('facility', row)
	
	String readmission = appoinmentData.getValue('readmission', row)
	
	String program = appoinmentData.getValue('program', row)
	
	String visitDate = appoinmentData.getValue('visitDate', row)
	
	String comment = appoinmentData.getValue('comment', row)

	//Call Test Case Login
	WebUI.callTestCase(findTestCase('WebTesting/1.2 - LoginWithValidData'), [:], FailureHandling.STOP_ON_FAILURE)
	
	//Select Facilities
	WebUI.selectOptionByValue(findTestObject('Object Repository/Page_CURA Healthcare Service/select_Tokyo CURA Healthcare Center        _5b4107 (1)'), 
	    facility, true)
	
	WebUI.delay(3)
	
	//Readmission Condition
	if (readmission.toLowerCase() == 'yes') {
		WebUI.click(findTestObject('Object Repository/WebTesting/Page_CURA Healthcare Service (1)/input_Apply for hospital readmission_hospit_63901f'))
	}else {
	}
	
	WebUI.delay(2)
	
	//Get Xpath For Dynamic Radio Button
	String dynamicXPath = ((('//input[@type = \'radio\' and @name = \'programs\' and @id = \'radio_program_' + program.toLowerCase()) + 
	'\' and @value = \'') + program) + '\']'
	
	TestObject radioButton = new TestObject()
	
	radioButton.addProperty('xpath', ConditionType.EQUALS, dynamicXPath)
	
	WebUI.click(radioButton)
	
	WebUI.delay(2)
	
	//Fill in Visit Date
	WebUI.setText(findTestObject('Object Repository/WebTesting/Page_CURA Healthcare Service (1)/input_Visit Date (Required)_visit_date'), 
	    visitDate)
	
	WebUI.delay(2)
	
	//Fill in Comment
	WebUI.setText(findTestObject('Object Repository/WebTesting/Page_CURA Healthcare Service (1)/textarea_Comment_comment'), 
	    comment)
	
	WebUI.delay(2)
	
	//Click Book Appointment
	WebUI.click(findTestObject('Object Repository/WebTesting/Page_CURA Healthcare Service (1)/button_Book Appointment'))
	
	WebUI.delay(2)
	
	//Verify Result
	String resultFacility = WebUI.getText(findTestObject('Object Repository/Page_CURA Healthcare Service/p_Hongkong CURA Healthcare Center'))
	
	WebUI.verifyEqual(resultFacility, facility)
	
	String resultProgram = WebUI.getText(findTestObject('Object Repository/Page_CURA Healthcare Service/p_Medicare'))
	
	WebUI.verifyEqual(resultProgram, program)
	
	String resultVisitDate = WebUI.getText(findTestObject('Object Repository/Page_CURA Healthcare Service/p_30062024'))
	
	WebUI.verifyEqual(resultVisitDate, visitDate)
	
	String resultComment = WebUI.getText(findTestObject('Object Repository/Page_CURA Healthcare Service/p_a'))
	
	WebUI.verifyEqual(resultComment, comment)
	
	String resultReadmission = WebUI.getText(findTestObject('Object Repository/Page_CURA Healthcare Service/p_Yes'))
	
	WebUI.verifyEqual(resultReadmission, readmission)
	
	//Exit Browser
	WebUI.closeBrowser()
}

