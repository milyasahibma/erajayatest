import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory

//Call Test Case Login
WebUI.callTestCase(findTestCase('WebTesting/1.2 - LoginWithValidData'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

//Click Appointment without input visit date
WebUI.click(findTestObject('Object Repository/WebTesting/Page_CURA Healthcare Service (1)/button_Book Appointment'))

WebUI.delay(2)

// Capture the validation message
boolean isVisitDateInputValid = WebUI.executeJavaScript('return document.getElementById("txt_visit_date").checkValidity();', null)

// Verify if the validation message appears
if (!isVisitDateInputValid) {
	println("Validation message 'Please fill out this field.' appears as expected.")
} else {
	println("No validation message appears.")
}

WebUI.delay(2)

//Exit Browser
WebUI.closeBrowser()