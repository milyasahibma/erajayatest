<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Medicare                               _cb3988</name>
   <tag></tag>
   <elementGuidId>6929cf90-ca9d-40f1-92e3-ddf9ceb24769</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//section[@id='appointment']/div/div/form/div[3]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>6b657401-1ea0-47bc-a09c-66f0ed128cbe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-sm-4</value>
      <webElementGuid>781c12a2-a012-4170-9ea4-0db7b38ad5cf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        
                             Medicare
                        
                        
                             Medicaid
                        
                        
                             None
                        
                    </value>
      <webElementGuid>4962fc2a-450a-4fae-97af-2925e7b469e2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;appointment&quot;)/div[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]/form[@class=&quot;form-horizontal&quot;]/div[@class=&quot;form-group&quot;]/div[@class=&quot;col-sm-4&quot;]</value>
      <webElementGuid>f2f48a51-aea8-482a-ae0a-f75f21a2927e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='appointment']/div/div/form/div[3]/div</value>
      <webElementGuid>fdde02b8-76cb-4c67-95aa-8a0dc25c31b5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Healthcare Program'])[1]/following::div[1]</value>
      <webElementGuid>eccaa5d4-2cd2-4630-aca9-a4b782a83d79</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div</value>
      <webElementGuid>57d3b9e0-d719-4f16-88d8-29e192904393</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '&#xd;
                        &#xd;
                             Medicare&#xd;
                        &#xd;
                        &#xd;
                             Medicaid&#xd;
                        &#xd;
                        &#xd;
                             None&#xd;
                        &#xd;
                    ' or . = '&#xd;
                        &#xd;
                             Medicare&#xd;
                        &#xd;
                        &#xd;
                             Medicaid&#xd;
                        &#xd;
                        &#xd;
                             None&#xd;
                        &#xd;
                    ')]</value>
      <webElementGuid>9205230a-64c7-4d34-88d9-e3ad8ea09b30</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
