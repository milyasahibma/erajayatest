<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Appointment Confirmation               _7d61e7</name>
   <tag></tag>
   <elementGuidId>409200a0-3a8e-45ff-a48e-84bbac8c9fe9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//section[@id='summary']/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.row</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>6133204a-caef-4403-8e77-423ee3828463</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>row</value>
      <webElementGuid>7f551ee6-ace5-41f3-bc18-84038cd8c086</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            
                Appointment Confirmation
                Please be informed that your appointment has been booked as following:
                
            
            
                
                    Facility
                
                
                    Tokyo CURA Healthcare Center
                
            
            
                
                    Apply for hospital readmission
                
                
                    Yes
                
            
            
                
                    Healthcare Program
                
                
                    Medicaid
                
            
            
                
                    Visit Date
                
                
                    30/06/2024
                
            
            
                
                    Comment
                
                
                    Testing Make Appointment
                
            
            
                Go to Homepage
            
        </value>
      <webElementGuid>0767c6fb-3a13-4768-972c-3af0ff94519f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;summary&quot;)/div[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]</value>
      <webElementGuid>57d6dad4-bddf-4090-8c96-e01ac90fbc52</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='summary']/div/div</value>
      <webElementGuid>6a218237-4f3b-4b5c-a176-dc9f00dbae0a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Make Appointment'])[1]/following::div[2]</value>
      <webElementGuid>8aa10c33-206c-4e5d-80bc-6756ed6c184d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='We Care About Your Health'])[1]/following::div[2]</value>
      <webElementGuid>2ec4530b-555c-42d7-98fe-a90a7945b628</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div</value>
      <webElementGuid>1795fbcc-41df-45e0-a5e5-fa3524e518cd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
            
                Appointment Confirmation
                Please be informed that your appointment has been booked as following:
                
            
            
                
                    Facility
                
                
                    Tokyo CURA Healthcare Center
                
            
            
                
                    Apply for hospital readmission
                
                
                    Yes
                
            
            
                
                    Healthcare Program
                
                
                    Medicaid
                
            
            
                
                    Visit Date
                
                
                    30/06/2024
                
            
            
                
                    Comment
                
                
                    Testing Make Appointment
                
            
            
                Go to Homepage
            
        ' or . = '
            
                Appointment Confirmation
                Please be informed that your appointment has been booked as following:
                
            
            
                
                    Facility
                
                
                    Tokyo CURA Healthcare Center
                
            
            
                
                    Apply for hospital readmission
                
                
                    Yes
                
            
            
                
                    Healthcare Program
                
                
                    Medicaid
                
            
            
                
                    Visit Date
                
                
                    30/06/2024
                
            
            
                
                    Comment
                
                
                    Testing Make Appointment
                
            
            
                Go to Homepage
            
        ')]</value>
      <webElementGuid>ca004de9-5243-49d9-be5d-f1bb24699a9b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
